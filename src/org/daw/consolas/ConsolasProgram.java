/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw.consolas;

import org.daw.consolas.clases.*;

/**
 *
 * @author rgcenteno
 */
public class ConsolasProgram {

    private static Consola c = null;
    
    public static void main(String[] args) {
        String opcion = "";
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        do{      
            if(c != null){
                c.ejecutar();
            }
            System.out.println("*****************************");
            System.out.println("* 1. Consola básica         *");
            System.out.println("* 2. Consola avanzada       *");
            System.out.println("* 3. Consola trigonómetrica *");
            System.out.println("*                           *");
            System.out.println("* 0. Salir                  *");
            System.out.println("*****************************");           
            
            opcion = teclado.nextLine();
            
            switch(opcion){
                case "1":
                    c = new Consola();
                    break;
                case "2":
                    c = new ConsolaAvanzada();
                    break;
                case "3":
                    c = new ConsolaTrigonometrica();
                    break;
                case "0":
                    break;
                default: 
                    System.out.println("Opción inválida"); 
                    c = null;
                    break;            
            }               
        }while(!opcion.equals("0"));
        
    }
    
}
