/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw.consolas.clases;

/**
 *
 * @author rgcenteno
 */
public class ConsolaAvanzada extends Consola{

    @Override
    public void mostrarMenu() {
        System.out.println("**********************************");
        System.out.println("* 1. Sumar dos números           *");
        System.out.println("* 2. Restar dos números          *");
        System.out.println("* 3. Multiplicar dos números     *");
        System.out.println("* 4. Dividir dos números         *");
        System.out.println("* 5. Elevar un número a otro     *");
        System.out.println("* 6. Raíz cuadrada de un número  *");
        System.out.println("*                                *");
        System.out.println("* 0. Salir                       *");
        System.out.println("**********************************");   
    }
    
    @Override
    public void ejecutar(){
        String opcion = "";
        do{
            mostrarMenu();
            opcion = teclado.nextLine();
            switch(opcion){
                case "1":
                    sumar();
                    break;
                case "2":
                    restar();
                    break;
                case "3":
                    multiplicar();
                    break;
                case "4":
                    dividir();
                case "5":
                    elevar();
                    break;
                case "6":
                    raiz();
                    break;
                case "0":
                    break;
                default: 
                    System.out.println("Opción inválida");
            }
        }
        while(!opcion.equals("0"));
    }    
    
    protected void elevar(){
        double a = solicitarDouble("Inserte la base", "Inserte un número");
        double b = solicitarDouble("Inserte el exponente", "Inserte un número");
        System.out.println("El resultado de " + a + "^" + b + " = " + Math.pow(a, b));
    }
    
    protected void raiz(){
        double a = solicitarDouble("Inserte el número", "Inserte un número");        
        System.out.println("La raíz de " + a + "es: " + Math.sqrt(a));
    }
    
}
