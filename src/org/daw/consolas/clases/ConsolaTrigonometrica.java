/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw.consolas.clases;

/**
 *
 * @author rgcenteno
 */
public class ConsolaTrigonometrica extends Consola{

    @Override
    public void mostrarMenu() {
        System.out.println("**********************************");
        System.out.println("* 1. Sumar dos números           *");
        System.out.println("* 2. Restar dos números          *");
        System.out.println("* 3. Multiplicar dos números     *");
        System.out.println("* 4. Dividir dos números         *");
        System.out.println("* 5. Tangente de un ángulo       *");
        System.out.println("* 6. Seno de un ángulo           *");
        System.out.println("* 7. Coseno de un ángulo         *");
        System.out.println("*                                *");
        System.out.println("* 0. Salir                       *");
        System.out.println("**********************************");   
    }
    
    @Override
    public void ejecutar(){
        String opcion = "";
        do{
            mostrarMenu();
            opcion = teclado.nextLine();
            switch(opcion){
                case "1":
                    sumar();
                    break;
                case "2":
                    restar();
                    break;
                case "3":
                    multiplicar();
                    break;
                case "4":
                    dividir();
                case "5":
                    tangente();
                    break;
                case "6":
                    seno();
                    break;
                case "7":
                    coseno();
                    break;  
                case "0":
                    break;
                default: 
                    System.out.println("Opción inválida");
            }
        }
        while(!opcion.equals("0"));
    }    
    
     protected void tangente(){
        double a = solicitarDouble("Inserte el ángulo en grados", "Inserte un número");        
        System.out.println("La tangente de " + a + "º = " + Math.tan(Math.toRadians(a)));
    }
     
    protected void seno(){
        double a = solicitarDouble("Inserte el ángulo en grados", "Inserte un número");        
        System.out.println("El seno de " + a + "º = " + Math.sin(Math.toRadians(a)));
    }
    
    protected void coseno(){
        double a = solicitarDouble("Inserte el ángulo en grados", "Inserte un número");        
        System.out.println("La tangente de " + a + "º = " + Math.cos(Math.toRadians(a)));
    }
}
