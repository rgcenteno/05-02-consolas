/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw.consolas.clases;

/**
 *
 * @author rgcenteno
 */
public class Consola {
    
    protected java.util.Scanner teclado = new java.util.Scanner(System.in);
    
    public void ejecutar(){
        String opcion = "";
        do{
            mostrarMenu();
            opcion = teclado.nextLine();
            switch(opcion){
                case "1":
                    sumar();
                    break;
                case "2":
                    restar();
                    break;
                case "3":
                    multiplicar();
                    break;
                case "4":
                    dividir();
                case "0":
                    break;
                default: 
                    System.out.println("Opción inválida");
            }
        }
        while(!opcion.equals("0"));
    }    
    
    public void mostrarMenu(){        
        System.out.println("**********************************");
        System.out.println("* 1. Sumar dos números           *");
        System.out.println("* 2. Restar dos números          *");
        System.out.println("* 3. Multiplicar dos números     *");
        System.out.println("* 4. Dividir dos números         *");
        System.out.println("*                                *");
        System.out.println("* 0. Salir                       *");
        System.out.println("**********************************");        
    }
    
    protected void sumar(){
        double a = solicitarDouble("Inserte el primer sumando", "Inserte un número");
        double b = solicitarDouble("Inserte el segundo sumando", "Inserte un número");
        System.out.println("La suma de " + a + " + " + b + " = " + (a + b));
    }
    
    protected void restar(){
        double a = solicitarDouble("Inserte el minuendo", "Inserte un número");
        double b = solicitarDouble("Inserte el sustraendo", "Inserte un número");
        System.out.println("La resta de " + a + " * " + b + " = " + (a - b));
    }
    
    protected void multiplicar(){
        double a = solicitarDouble("Inserte el primer número", "Inserte un número");
        double b = solicitarDouble("Inserte el segundo número", "Inserte un número");
        System.out.println("La resta de " + a + " * " + b + " = " + (a * b));
    }
    
    protected void dividir(){
        double a = solicitarDouble("Inserte el dividendo", "Inserte un número");
        double b = solicitarDouble("Inserte el divisor", "Inserte un número");
        System.out.println("La division de " + a + " / " + b + " = " + (a / b));
    }
    
    protected double solicitarDouble(String texto, String textoError){
        Double a = null;
        System.out.println(texto);
        do{
            if(teclado.hasNextDouble()){
                a = teclado.nextDouble();
            }
            else{
                System.out.println(textoError);
            }
            teclado.nextLine();
        }
        while(a == null);
        return a;
    }
}
